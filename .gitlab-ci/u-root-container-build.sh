#!/bin/bash
set -ex

. .gitlab-ci/build_functions.sh

build() {
    case $CONTAINER_VARIANT in
        "linux/amd64")
            GOARCH=amd64
            ;;
        "linux/arm64/v8")
            GOARCH=arm64
            dnf install -y qemu-user-static-aarch64
            ;;
        "linux/arm/v6")
            GOARCH=arm
            GOARM=6
            dnf install -y qemu-user-static-arm
            ;;
        "linux/riscv64")
            GOARCH=riscv64
            dnf install -y qemu-user-static-riscv
            ALPINE_VERSION=edge  # TODO: Remove this when 3.18 gets released
            ;;
        *)
            echo "Unsupported platform!"
            exit 1
            ;;
    esac

    buildah build -f Dockerfile -t $IMAGE_NAME --build-arg ALPINE_VERSION=$ALPINE_VERSION --build-arg GOARCH=$GOARCH --build-arg GOARM=${GOARM:-} --platform $CONTAINER_VARIANT --net host .
}

build_and_push_container
