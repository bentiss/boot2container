source ./tests/integration/base.sh

testBootingWithNoNICNoDisk() {
    qemu_params="$QEMU_NO_NIC"
    kernel_cmdline="b2c.run=docker://docker.io/library/hello-world:latest"
    run_test

    assertContains "$stdout" "ERROR: Could not connect to the network, shutting down!"
    assertNotContains "$stdout" "Execution is over, pipeline status:"
}
suite_addTest testBootingWithNoNICNoDisk

testBootingWithNoNICNoDiskAndCacheDeviceAuto() {
    qemu_params="$QEMU_NO_NIC"
    kernel_cmdline="b2c.cache_device=auto b2c.run=docker://docker.io/library/hello-world:latest"
    run_test

    assertContains "$stdout" "ERROR: Could not connect to the network, shutting down!"
    assertContains "$stdout" "No disks founds, continue without a cache"
    assertNotContains "$stdout" "Selected the partition"
    assertNotContains "$stdout" "Execution is over, pipeline status:"
}
suite_addTest testBootingWithNoNICNoDiskAndCacheDeviceAuto

testBootingWithDefault() {
    qemu_params="$QEMU_NIC $QEMU_DISK"
    kernel_cmdline="b2c.run=docker://docker.io/library/hello-world:latest"
    reset_disk; disk_hash=`md5sum "/disk.img"`
    run_test

    assertContains "$stdout" "Do not use a partition cache"
    assertContains "$stdout" "WARNING: Did not reset the time, use b2c.ntp_peer=auto to set it on boot"
    assertContains "$stdout" "Hello from Docker!"
    assertContains "$stdout" "(1/3): Execution is over, pipeline status: 0"
    assertContains "$stdout" "(2/3): Execution is over, pipeline status: 0"
    assertContains "$stdout" "(3/3): Execution is over, pipeline status: 0"

    assertEquals "The disk content has been modified" "$disk_hash" "`md5sum "/disk.img"`"
}
suite_addTest testBootingWithDefault

testBootingWithCacheDevice() {
    # Check that a cache drive gets formated and mounted
    qemu_params="$QEMU_NIC $QEMU_DISK"
    kernel_cmdline="b2c.cache_device=auto"
    reset_disk; disk_hash=`md5sum "/disk.img"`
    run_test
    assertContains "$stdout" "No existing cache partition found on this machine, create one from the disk /dev/vda"
    assertContains "$stdout" "mkfs.ext4 -O encrypt -F -L B2C_CACHE /dev/vda1"
    assertContains "$stdout" "Selected the partition /dev/vda1 as a cache"
    assertContains "$stdout" "Mounting the partition /dev/vda1 to /storage: DONE"
    assertNotContains "$stdout" "Mounting a swap file"
    assertNotEquals "Disk_1's content has not been modified" "$disk_hash" "`md5sum "/disk.img"`"
    assertContains "$stdout" "Remounting the partition /dev/vda1 read-only: DONE"

    # Check that if we add a new disk, the one already formated is used preferably
    fallocate -l 128M "/disk2.img"
    disk2_hash=`md5sum "/disk2.img"`
    qemu_params="$QEMU_NIC -drive file=/disk2.img,format=raw,if=virtio $QEMU_DISK"
    kernel_cmdline="b2c.cache_device=auto"
    run_test
    assertNotContains "$stdout" "mkfs.ext4 -O encrypt -F -L B2C_CACHE /dev/vda"
    assertContains "$stdout" "Selected the partition /dev/vdb1 as a cache"
    assertNotContains "$stdout" "Mounting a swap file"
    assertEquals "The disk_2 content has been modified" "$disk2_hash" "`md5sum "/disk2.img"`"
    assertContains "$stdout" "Remounting the partition /dev/vdb1 read-only: DONE"

    # Force-use the second disk, and make sure the original cache disk isn't being touched
    kernel_cmdline="b2c.cache_device=/dev/vda"
    disk_hash=`md5sum "/disk.img"`
    disk2_hash=`md5sum "/disk2.img"`
    run_test
    assertContains "$stdout" "No existing cache partition on the drive /dev/vda, recreate the partition table and format a partition"
    assertContains "$stdout" "mkfs.ext4 -O encrypt -F -L B2C_CACHE /dev/vda1"
    assertContains "$stdout" "Selected the partition /dev/vda1 as a cache"
    assertContains "$stdout" "Mounting the partition /dev/vda1 to /storage: DONE"
    assertNotContains "$stdout" "Mounting a swap file"
    assertEquals "Disk_1's content has been modified" "$disk_hash" "`md5sum "/disk.img"`"
    assertNotEquals "The disk_2 content has not been modified" "$disk2_hash" "`md5sum "/disk.img"`"
    assertContains "$stdout" "Remounting the partition /dev/vda1 read-only: DONE"

    # Try configuring the cache device using b2c.filesystem
    kernel_cmdline='b2c.filesystem="vdb1,src=/dev/vdb1" b2c.cache_device=vdb1 b2c.swap=64M'
    disk_hash=`md5sum "/disk.img"`
    disk2_hash=`md5sum "/disk2.img"`
    run_test
    assertContains "$stdout" "+ mount /dev/vdb1 /storage"
    assertContains "$stdout" "Mounted the vdb1 b2c.filesystem as a cache device"
    assertContains "$stdout" "Mounting a swap file of 64M: DONE"
}
suite_addTest testBootingWithCacheDevice

testSwapFile() {
    # Check that a cache drive gets formated and mounted
    qemu_params="$QEMU_NIC $QEMU_DISK"
    kernel_cmdline="b2c.cache_device=auto b2c.swap=64M"
    reset_disk; disk_hash=`md5sum "/disk.img"`
    run_test
    assertContains "$stdout" "Mounting a swap file of 64M"
    assertContains "$stdout" "Unmounting the swap file: DONE"
}
suite_addTest testSwapFile

testNTP() {
    # Check that 'auto' defaults to pool.ntp.org
    qemu_params="$QEMU_NIC"
    kernel_cmdline="b2c.ntp_peer=auto"
    run_test
    assertContains "$stdout" "Getting the time from the NTP server pool.ntp.org: DONE"

    # TODO: Check that the time was indeed updated

    # Check overriding the ntp peer works
    qemu_params="$QEMU_NIC"
    kernel_cmdline="b2c.ntp_peer=wrong_server.com"
    run_test
    assertContains "$stdout" "Getting the time from the NTP server wrong_server.com: FAILED"
}
suite_addTest testNTP

testPoweroffDelay() {
    qemu_params="$QEMU_NIC"
    kernel_cmdline='b2c.poweroff_delay=0.1234'
    run_test
    assertContains "$stdout" "sleep 0.1234"
    assertNotContains "$stdout" "/bin/true"
}
suite_addTest testPoweroffDelay

testShutdownCmdOverride() {
    # Use printf to concatenate the strings, so we don't accidentally find the
    # the parameter from the kernel command line and think it actually got executed!
    qemu_params="$QEMU_NIC"
    kernel_cmdline='b2c.shutdown_cmd="printf %s%s%s\\n SHUT ME DOWN; poweroff -f"'
    run_test

    assertContains "$stdout" "(1/3): It's now safe to turn off your computer"
    assertContains "$stdout" "(2/3): It's now safe to turn off your computer"
    assertContains "$stdout" "(3/3): It's now safe to turn off your computer"
    assertContains "$stdout" "SHUTMEDOWN"
}
suite_addTest testShutdownCmdOverride
