#!/bin/busybox sh

suite()
{
  UNITTEST="${UNITTEST:-1}"
  INTEGRATION="${INTEGRATION:-1}"
  VM2C="${VM2C:-1}"

  # Unittests
  if [ "$UNITTEST" -eq 1 ]; then
    . ./tests/unittests/cache_device.sh
    . ./tests/unittests/container.sh
    . ./tests/unittests/cmdline.sh
    . ./tests/unittests/hooks.sh
    . ./tests/unittests/misc.sh
    . ./tests/unittests/network.sh
    . ./tests/unittests/volumes.sh
  fi

  # Integration
  if [ "$INTEGRATION" -eq 1 ]; then
    . ./tests/integration/misc.sh
    . ./tests/integration/containers.sh
    . ./tests/integration/volumes.sh
  fi

  # vm2c
  if [ "$VM2C" -eq 1 ]; then
    . ./tests/vm2c/vm2c.sh
  fi
}

# HACK: Make sure the initscript knows where to find the run_cmd_in_loop command
cp ./run_cmd_in_loop.sh /bin/run_cmd_in_loop.sh

# Load shUnit2.
stdout=""
cmdline=""
exit_code=""
. /usr/bin/shunit2
