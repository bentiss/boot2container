# WARNING: Only the firmwares for newer GPUs are shipped automatically
# This is because we otherwise hit the following kernel compilation issue
# make[5]: /bin/sh: Argument list too long
# See https://superuser.com/questions/1677576/add-all-firmware-to-config-extra-firmware

CONFIG_DRM=y
CONFIG_HAS_DMA=y
CONFIG_HAS_IOMEM=y

<% if [[ "${arch}" == "amd64" ]]; then -%>

CONFIG_AGP=y

# Simple VESA-/UEFI-backed framebuffer exposed as a DRM driver
CONFIG_DRM_SIMPLEDRM=y
CONFIG_SYSFB_SIMPLEFB=y

# AMDGPU
CONFIG_DRM_AMDGPU=y
CONFIG_HSA_AMD=y
CONFIG_DRM_AMD_DC=y
CONFIG_DRM_AMD_ACP=y
CONFIG_DRM_AMDGPU_USERPTR=y
# B2C_FW+=amdgpu/*

# Nouveau
CONFIG_DRM_NOUVEAU=y
# B2C_FW+=nvidia/**/*

# i915
CONFIG_AGP_INTEL=y
CONFIG_DRM_I915=y
# NOTE: Only include the DMC firmwares for now, as the GuC and HuC don't really bring any features
# This will however change with the arrival of DG2 which will require the GuC
# B2C_FW+=i915/*dmc*.bin

# Radeon (older AMD GPUs): Disabled because we cannot embed the firmwares...
#CONFIG_DRM_RADEON=y
## B2C_FW+=radeon/*

<% elif [[ "${arch}" == "arm64" ]]; then -%>

CONFIG_FB_SIMPLE=y

# Broadcom GPU
CONFIG_SOUND=y
CONFIG_SND=y
CONFIG_SND_SOC=y
CONFIG_SND_DRIVERS=n
CONFIG_SND_PCI=n
CONFIG_SND_USB=n
CONFIG_DRM_VC4=y
CONFIG_DRM_V3D=y

# Disable dGPU support
CONFIG_DRM_AMDGPU=n
CONFIG_DRM_NOUVEAU=n

<% elif [[ "${arch}" == "arm" ]] || [[ "${arch}" == "riscv64" ]]; then -%>
# Disable dGPU support
CONFIG_DRM_RADEON=n
CONFIG_DRM_AMDGPU=n
CONFIG_DRM_NOUVEAU=n
<% fi -%>

# Qemu
<% if [[ "$features" != *"qemu"* ]]; then -%>
# VirtIO
CONFIG_VIRTIO_MENU=y
CONFIG_DRM_VIRTIO_GPU=y
<% fi -%>

# VMWare
<% if [[ "${arch}" != "arm" ]] && [[ "${arch}" != "riscv64" ]]; then -%>
CONFIG_DRM_VMWGFX=y
<% fi -%>
<% if [[ "${arch}" == "amd64" ]]; then -%>
CONFIG_DRM_VMWGFX_MKSSTATS=y
<% fi -%>
