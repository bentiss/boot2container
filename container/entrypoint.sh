#!/bin/sh

set -eux

# TODO: By calling the initscript directly, we could save quite a bit of
# size, but we first need to study what the init program does for us :)
#-initcmd="/bin/initscript.sh" -nocmd

echo "${B2C_VERSION:-unknown}" > /tmp/b2c.version

/src/u-root/u-root -uroot-source /src/u-root -files /bin/sh -defaultsh="" \
-files ./crun-no-pivot:bin/crun-no-pivot \
-files /usr/bin/conmon \
-files /usr/bin/myunshare \
-files /bin/podman \
-files /usr/lib/podman/netavark \
-files /usr/lib/libgcc_s.so.1 \
-files /bin/fscryptctl \
-files ./config/containers:etc/containers \
-files ./config/cni:etc/cni \
-files ./config/keymaps:usr/share/keymaps \
-files /tmp/b2c.version:etc/b2c.version \
-files /usr/bin/crun:bin/crun \
-files /sbin/mkfs.ext4 -files /usr/sbin/parted:sbin/parted -files /bin/lsblk \
-files /etc/ssl/certs/ca-certificates.crt \
-files ./uhdcp-default.sh:etc/uhdcp-default.sh \
-files ./initscript.sh:bin/initscript.sh \
-files ./run_cmd_in_loop.sh:bin/run_cmd_in_loop.sh \
-uinitcmd="/bin/initscript.sh" cmds/core/init cmds/core/wget /src/mc/
